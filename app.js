require('./bootstrap');

Array.prototype.groupBy = function(prop) {
    return this.reduce(function(groups, item) {
        var val = item[prop];
        groups[val] = groups[val] || [];
        groups[val].push(item);
        return val;
    }, {});
}

// window.Vue = require('vue');
import Vue from 'vue/dist/vue';
import VueCarousel from 'vue-carousel';
import VueTouch from 'vue-touch';
import Vuex from 'vuex';
import VuePreview from './components/component/preview';
import * as vMediaQuery from 'v-media-query';
import vueScrollto from 'vue-scrollto';
import Autocomplete from 'v-autocomplete';
import VTooltip from 'v-tooltip';
import VeeValidate from 'vee-validate';
// import VueTheMask from 'vue-the-mask';

const moment = require('moment');
require('moment/locale/ru');
const VueInputMask = require('vue-inputmask').default;

Vue.use(VueCarousel);
Vue.use(VueTouch);
Vue.use(require('vue-moment'), {moment});
Vue.use(Vuex);
Vue.use(VuePreview);
Vue.use(vMediaQuery.default);
Vue.use(vueScrollto);
Vue.use(Autocomplete);
Vue.use(VueInputMask);
Vue.use(VTooltip);
Vue.use(VeeValidate);
// Vue.use(VueTheMask);

import Headercmp from './components/component/Header.vue';
// import Cart from './components/component/Cart.vue';
import Navbar from './components/component/NavBar.vue';
import Banner from './components/component/Banner.vue';
import CategoryImage from './components/component/CategoryImage.vue';
import LastProducts from './components/component/LastEightProducts.vue';
// import LastArticles from './components/component/LastArticles.vue';
import SubscribeEmail from './components/component/SubscribeEmail.vue';
import SubscribeFooter from './components/component/SubscribeFooter.vue';
import Social from './components/component/Social.vue';
import ModalLogin from './components/component/ModalLogin.vue';
import ModalRegister from './components/component/ModalRegister.vue';
import ModalForget from './components/component/ModalForget.vue';
import Breadcrumbs from './components/component/Breadcrumbs.vue';
import Categories from './components/component/Categories.vue';
import ProductCategory from './components/component/ProductCategory.vue';
import Filters from './components/component/Filters.vue';
import Articles from './components/component/Articles.vue';
import ModalReview from './components/component/ModalReview.vue';
import ModalThanks from './components/component/ModalThanks.vue';
import ProductsAll from './components/component/ProductsAll.vue';
import ProductCard from './components/component/ProductCard.vue';
import ProductBuy from './components/component/product-card/ProductBuy.vue';
import SingleArticle from './components/component/SingleArticle.vue';
import UserPage from './components/component/user/UserPage.vue';
import ModalPassword from './components/component/ModalPassword.vue';
import UserInfo from './components/component/user/UserInfo.vue';
import UserOrders from './components/component/user/UserOrders.vue';
import UserFavorite from './components/component/user/UserFavorite.vue';
import UserSubscription from './components/component/user/UserSubscription.vue';
import UserComments from './components/component/user/UserComments.vue';
import Shops from './components/component/static-pages/Shops.vue';
import Offers from './components/component/static-pages/Offers.vue';
import About from './components/component/static-pages/About.vue';
import Checkout from './components/component/Checkout.vue';
import Contacts from './components/component/static-pages/Contacts.vue';
import Help from './components/component/static-pages/Help.vue';
import ModalMessage from './components/component/ModalMessage.vue';
import ModalSubscribe from './components/component/ModalSubscribe.vue';
import ModalSize from './components/component/ModalSize.vue';
import ModalOrder from './components/component/ModalOrder.vue';
import ModalSingleProduct from './components/component/ModalSingleProduct.vue';
import ModalSingleOrder from './components/component/ModalSingleOrder.vue';
import FooterMenu from './components/component/FooterMenu.vue';
import Delivery from './components/component/static-pages/Delivery.vue';
import Payment from './components/component/Payment.vue';
import TwitterVerify from './components/component/adds/TwitterVerify.vue';
import Cart from './components/component/Cart.vue';
import Search from './components/component/Search.vue';
import HomeSeo from './components/component/HomeSeo.vue';

Vue.component('headercmp', Headercmp);
Vue.component('cart', Cart);
Vue.component('search', Search);
Vue.component('banner', Banner);
Vue.component('lastproducts', LastProducts);
// Vue.component('lastarticles', LastArticles);
Vue.component('subscribeemail', SubscribeEmail);
Vue.component('social', Social);
Vue.component('subscribe-footer', SubscribeFooter);
Vue.component('modal-login', ModalLogin);
Vue.component('modal-register', ModalRegister);
Vue.component('modal-review', ModalReview);
Vue.component('modal-forget', ModalForget);
Vue.component('modal-thanks', ModalThanks);
Vue.component('breadcrumbs', Breadcrumbs);
Vue.component('categories', Categories);
Vue.component('category-image', CategoryImage);
Vue.component('product-category', ProductCategory);
Vue.component('filters', Filters);
Vue.component('products-all', ProductsAll);
Vue.component('product-card', ProductCard);
Vue.component('product-buy', ProductBuy);
Vue.component('articles', Articles);
Vue.component('single-article', SingleArticle);
Vue.component('user-page', UserPage);
Vue.component('modal-password', ModalPassword);
Vue.component('user-info', UserInfo);
Vue.component('user-orders', UserOrders);
Vue.component('user-favorite', UserFavorite);
Vue.component('user-subscription', UserSubscription);
Vue.component('user-comments', UserComments);
Vue.component('shops', Shops);
Vue.component('offers', Offers);
Vue.component('about', About);
Vue.component('checkout', Checkout);
Vue.component('contacts', Contacts);
Vue.component('help', Help);
Vue.component('modal-message', ModalMessage);
Vue.component('modal-subscribe', ModalSubscribe);
Vue.component('modal-single-product', ModalSingleProduct);
Vue.component('modal-single-order', ModalSingleOrder);
Vue.component('modal-size', ModalSize);
Vue.component('footer-menu', FooterMenu);
Vue.component('modal-order', ModalOrder);
Vue.component('delivery', Delivery);
Vue.component('payment', Payment);
Vue.component('twitter-verify', TwitterVerify);
Vue.component('home-seo', HomeSeo);

import {cartModule} from './shop/cart.js';
import {userModule} from './shop/user.js';
import {storeModule} from './shop/store.js';

const store = new Vuex.Store({
   modules:{
       cart: cartModule,
       user: userModule,
       bread: storeModule
   }
});
const app = new Vue({
    store,
    el: '#app',
    components: {Headercmp, Navbar, Banner},
    // render(createElement){
    //     return createElement('div',{},[
    //         createElement(Headercmp),
    //         createElement(Navbar),
    //         createElement(Banner)
        // ]);
    // }
});
    // .$mount('#app');