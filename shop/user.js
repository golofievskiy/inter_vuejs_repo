export const userModule = {
    state:{
        user: null
    },
    actions:{
        login({commit}, userInfo){
            // commit('GET_LOGIN', userInfo);
            // let item = state;
            return new Promise((resolve, reject)=>{
                axios.post('login', {email: userInfo.email, password: userInfo.password }).then(response=>{
                    commit('GET_LOGIN', response.data);
                    resolve(response.data);
                }).catch(err=>reject(err));
            });
        },
        logout({commit}){
            commit('GET_LOGOUT');
        },
        register({commit}, userInfo){
            return new Promise((resolve, reject)=>{
                axios.post('auth/create', userInfo).then(response => {
                    // commit('GET_LOGIN', response.data);
                    resolve(response.data);
               }).catch(error=> {reject(error);});
            })
        },
        pasteUser({commit}, userInfo){
            commit('PASTE_USER', userInfo);
        }
    },
    mutations:{
        GET_LOGIN(state, userInfo){
            state.user = userInfo;
        },
        GET_LOGOUT(state){
            axios.get('logout').then(res=>{
                if(res.data.status){
                    state.user = null;
                    if(window.location.pathname == '/user'){
                        window.location.replace('/');
                    }
                }
            });
            // state.user = null;
        },
        PASTE_USER(state, userInfo){
            if(Array.isArray(userInfo)){
                console.log(userInfo);
                userInfo = null;
            }
            state.user = userInfo;
        }
    },
    getters:{
        loginStatus(state){
            return state.user;
        }
    }
};