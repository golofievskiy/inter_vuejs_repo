export const cartModule = {
    state:{
        // products: {}
        amount: 0,
        product_count: 0,
        products: [],
        hasChanges: false
    },
    actions:{
        addProduct({commit}, product){
            return new Promise((resolve, reject)=>{
                axios.post('order/add',product).then(res=>{
                    console.log(res.data);
                    commit('ADD_PRODUCT', res.data);
                    resolve('Товар добавлен в корзину!');
                    // state.amount = res.data.current_order.amount;
                    // state.product_count = res.data.current_order.product_count;
                    // state.products = res.data.products;
                }).catch(err=>{
                    console.log(err);
                    reject('Ошибка');
                });
            });

        },
        setProducts({commit}, items){
            commit('SET_PRODUCTS', items);
        },
        changeQuantity({commit}, product){
            console.log(product);
            commit('CHANGE_QUANTITY', product);
        },
        removeProduct({commit}, item){
            commit('REMOVE_PRODUCT', item);
        },
        changeAmount({commit}, item){
            commit('CHANGE_AMOUNT', item);
        },
        setDiscount({commit}, items){
            commit('SET_DISCOUNT', items);
        }
    },
    mutations:{
        SET_DISCOUNT(state, items){
            state.products.forEach(el=>{
                 items.forEach(obj=>{
                     if(el.product_attribute_id == obj.product_attribute_id && obj.price != el.price){
                         el.new_price = obj.price;
                         state.hasChanges = true;
                     }
                 })
            });
        },
        CHANGE_AMOUNT(state, item){
            state.amount = item;
        },
        REMOVE_PRODUCT(state, product){
            axios.get(`order/${product.id}/delete`).then(res=>{
                let index = -1;
                state.products.forEach((el,i)=>{
                    if(el.id == product.id){
                        index = i;
                    }
                });
                state.products.splice(index, 1);
                state.amount = res.data.amount;
                state.product_count = res.data.product_count;
                console.log(state.products,product.id,res);
            }).catch(err=>console.log(err));
        },
        CHANGE_QUANTITY(state, product){
            // console.log(product);
            axios.post(`order/change-quantity/${product.product.id}`, {product_attribute_id: product.product.product_attribute_id, quantity: product.quantity}).then(res=>{
                state.amount = res.data.amount;
                state.product_count = res.data.product_count;
                console.log(state.products);
                state.products.forEach((el,i)=>{
                    if(el.product_attribute_id == product.product.product_attribute_id){
                        el.quantity = product.quantity;
                    }
                });
            }).catch(err=>console.log(err));
        },
        ADD_PRODUCT(state, product){
            // axios.post('order/add',product).then(res=>{
            //     console.log(res.data);
            //     state.amount = res.data.current_order.amount;
            //     state.product_count = res.data.current_order.product_count;
            //     state.products = res.data.products;
            // }).catch(err=>console.log(err));
            state.amount = product.current_order.amount;
            state.product_count = product.current_order.product_count;
            state.products = product.products;
        },
        SET_PRODUCTS(state, product){
            if(!Array.isArray(product)){
                state.amount = product.current_order.amount;
                state.product_count = product.current_order.product_count;
                state.products = product.products;
            }


                // state.products['amount'] = products.current_order.amount;
                // state.products['product_count'] = products.current_order.product_count;
                // state.products['products'] = products.products;
                // state.products.amount = product.current_order.amount;
                // state.products.products = product.products;
                // state.products.product_count = product.current_order.product_count;
            // }
        }
    },
    getters:{
        amount(state){
            return state.amount;
        },
        product_count(state){
            return state.product_count;
        },
        products(state){
            return state.products;
        },
        is_changed(state){
            return state.hasChanges;
        }
    }
};