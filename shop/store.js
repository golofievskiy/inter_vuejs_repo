/**
 * Created by rated on 08.10.2017.
 */
export const storeModule = {
    state:{
        title: '',
        products: []
    },
    actions:{
        changeTitle({commit}, title){
            commit('CHANGE_TITLE', title);
        },
        setActiveProducts({commit}, products){
            commit('SET_ACTIVE_PRODUCTS', products);
        },
        changeProducts({commit}, products){
            commit('CHANGE_PRODUCTS', products);
        }
    },
    mutations:{
        CHANGE_TITLE(state, title){
            state.title = title
        },
        SET_ACTIVE_PRODUCTS(state, products){
            console.log(products);
            state.products = products;
        },
        CHANGE_PRODUCTS(state, products){
            state.products = products.data.product.data;
        }
    },
    getters:{
        getTitle(state){
            return state.title;
        },
        getProducts(state){
            return state.products;
        }
    }
};